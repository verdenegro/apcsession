<?php
namespace Verdenegro\ApcSession;

use Yii;
use yii\web\MultiFieldSession;

class ApcSession extends MultiFieldSession {
    /**
     * Updates the current session ID with a newly generated one .
     * Please refer to <http://php.net/session_regenerate_id> for more details.
     * @param bool $deleteOldSession Whether to delete the old associated session file or not.
     */
    public function regenerateID($deleteOldSession = false)
    {
        $oldID = session_id();

        // if no session is started, there is nothing to regenerate
        if (empty($oldID)) {
            return;
        }

        parent::regenerateID(false);
        $newID = session_id();
        // if session id regeneration failed, no need to create/update it.
        if (empty($newID)) {
            Yii::warning('Failed to generate new session ID', __METHOD__);
            return;
        }

        $oldData = apc_fetch('vnApcSession_'.$oldID, $inCache);
        if ( $inCache ) {
            $this->destroySession($oldID);

            $data = $oldData['data'];
        } else $data = false;

        $this->writeSession($newID, $data);
    }

    /**
     * Session read handler.
     * Do not call this method directly.
     * @param string $id session ID
     * @return string the session data
     */
    public function readSession($id)
    {
        $fields = apc_fetch('vnApcSession_'.$id, $inCache);
        if ( !$inCache ) return '';

        if ($this->readCallback !== null) {
            return $fields === false ? '' : $this->extractData($fields);
        } else {
            return $fields === false ? '' : $fields['data'];
        }
    }

    /**
     * Session write handler.
     * Do not call this method directly.
     * @param string $id session ID
     * @param string $data session data
     * @return bool whether session write is successful
     */
    public function writeSession($id, $data)
    {
        $fields = $this->composeFields($id, $data);
        $now = time();
        $expire = $fields['expire'];
        apc_store('vnApcSession_'.$id, $fields, ($expire - $now)) ;

        return true;
    }

    /**
     * Session destroy handler.
     * Do not call this method directly.
     * @param string $id session ID
     * @return bool whether session is destroyed successfully
     */
    public function destroySession($id)
    {
        apc_delete('vnApcSession_'.$id);

        return true;
    }

    /**
     * Session GC (garbage collection) handler.
     * Do not call this method directly.
     * @param int $maxLifetime the number of seconds after which data will be seen as 'garbage' and cleaned up.
     * @return bool whether session is GCed successfully
     */
    public function gcSession($maxLifetime)
    {
        return true;
    }
}